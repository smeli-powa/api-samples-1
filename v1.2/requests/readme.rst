Overview
-----------
This repo represents the JSON requests PowaTag will send out.  The expected responses are found in ../responses/

Get Product
-----------
Used to fetch product information from the ecomm platform. 
Method: GET

Ex: `https://api.yoursite.com/products/{productCode}?email=<email>&zip=<zip>&latitude=45.123&longitude=-0.08234&campaignid=12312&loyaltyid=45234&currency=USD&lang=en-US`


Calculate Costs
---------------------------------
Used to calculate the shipping costs and the taxes corresponding to a set of products and a user. Normally the ecom platform translates this action into the creation of a shopping cart. 
Method: POST 

Ex: `https://api.yoursite.com/orders/costs`


Insert Order
-------------
Used to process an order. When run in a split payment transaction, the associated order does not contain payment information.
Method: POST

Ex: `https://api.yoursite.com/orders`


Confirm Order Payment
---------------------
Used with split payments to confirm that an order has been properly processed by the PSP. In case of an unsuccesful payment, the statusCode and message fields should contain the appropriate info for the ecomm platform to know that the payment didn't go through. 
Method: POST

Ex: `https://api.yoursite.com/orders/{orderId}/confirm-payment` 

